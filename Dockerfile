FROM alpine:3.10 AS run
RUN apk --no-cache add jq curl bc tzdata findutils coreutils
ENV TZ=Europe/Berlin

FROM alpine:3.10 AS git
RUN apk --no-cache add git
